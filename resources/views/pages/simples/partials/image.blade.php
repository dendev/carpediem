@if( $page->image )
    <div class="row">
        <div class="col-sm-12 text-justify">
            <img alt="image illustration de la page" class="main-img" src="{{$page->image}}" />
        </div>
    </div>
@endif
