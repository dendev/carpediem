<?php
/* why !?
$mth = $column['name'];
try
{
    $users = $entry->$mth; // column name is name of relation
}
catch(\Exception $e)
{
    $users = $entry->$mth(); // column name is name of method
}
*/
$users = $entry->get_users();
$users = is_null($users) ? collect() : $users->sortBy('title');
$list_btn_label = array_key_exists('btn_label', $column) ? $column['btn_label'] : 'Liste';
$list_nothing_msg = array_key_exists('nothing_msg', $column) ? $column['nothing_msg'] : 'Vide';
?>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary w-100" data-toggle="modal" data-target="#listUserModal">
    {{$list_btn_label}}
</button>

<!-- Modal -->
<div class="modal fade" id="listUserModal" tabindex="-1" role="dialog" aria-labelledby="listUserModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="listUserModalTitle">{{$list_btn_label}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if( count( $users ) > 0 )
                    <table class="w-100">
                        @foreach($users as $user )
                            <tr>
                                <td width="70%">{{$user->name}}</td>
                                <td width="30%">
                        <span class="dropdown-item">
                            <a class="" href="{{route('user.edit', ['id' => $user->id])}}" target="_blank"> Afficher</a>
                            <!-- <a href="javascript:void(0)" onclick="deleteEntry({{$user->id}})" data-route="http://portail2.local/admin/group/1" class="btn btn-sm btn-link" data-button-type="delete"><i class="la la-trash"></i> Supprimer</a> -->
                        </span>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <a class="dropdown-item disabled" href="#">
                        {{$list_nothing_msg}}
                    </a>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
