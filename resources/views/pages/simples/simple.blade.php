@extends('layouts.app')

@section('content')
    @include('pages.simples.partials.title')
    @include('pages.simples.partials.image')
    @include('pages.simples.partials.content')
@endsection
