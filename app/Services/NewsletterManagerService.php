<?php
namespace App\Services;

use App\Models\News;
use App\Models\Newsletter;
use App\Models\Subscription;
use App\Models\User;
use App\Traits\UtilService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class NewsletterManagerService
{
    use UtilService;

    public function test_me()
    {
        return 'newsletter_manager';
    }

    public function add_user($email, $newsletter_identity = 'default')
    {
        $user = false;

        // create user
        $already_exist = User::where('email', $email)->first();

        if( ! $already_exist )
        {
            $user = new User();
            $user->name = $email;
            $user->email = $email;
            $user->password = Hash::make( env('USER_PASSWORD'));
            $user->save();
        }
        else
        {
            $user = $already_exist;
        }

        // make association // TODO use add_subscription
        $newsletter = Newsletter::where('identity', $newsletter_identity)->first();

        $already_exist = Subscription::where('user_id', $user->id)
            ->where('newsletter_id', $newsletter->id, )
            ->first();

        if( ! $already_exist)
        {
            $subscriber = new Subscription();
            $subscriber->user_id = $user->id;
            $subscriber->newsletter_id = $newsletter->id;
            $subscriber->save();
        }

        return $user;
    }

    public function add_subscription($id_or_model_user, $id_or_model_newsletter = false)
    {
        $subscription = false;

        $user = $this->_instantiate_if_id($id_or_model_user, User::class);
        if( $id_or_model_newsletter)
            $newsletter = $this->_instantiate_if_id($id_or_model_newsletter, Newsletter::class);
        else
            $newsletter = Newsletter::where('identity', 'default')->first();

        if( $user && $newsletter )
        {
            $already_exist = Subscription::where('user_id', $user->id)
                ->where('newsletter_id', $newsletter->id)
                ->where('is_active', true)
                ->first();

            if( $already_exist )
            {
                $subscription = $already_exist;
            }
            else
            {
                $subscription = new Subscription();
                $subscription->user_id = $user->id;
                $subscription->newsletter_id = $newsletter->id;
                $subscription->is_active = true;
                $subscription->save();
            }
        }

        return $subscription;
    }

    public function create($title, $description)
    {
        $newsletter = false;

        $already_exist = Newsletter::where('title', $title)->first();
        if( $already_exist)
        {
            $news  = $already_exist;
        }
        else
        {
            $newsletter = new Newsletter();
            $newsletter->title = $title;
            $newsletter->description = $description;
            $newsletter->save();
        }

        return $newsletter;
    }

    public function create_news($title, $content, $newsletter_identity = 'default')
    {
        $already_exist = News::where('title', $title)->first();
        if( $already_exist)
        {
            $news  = $already_exist;
        }
        else
        {
            $newsletter = Newsletter::where('identity', $newsletter_identity)->first();

            $news = new News();
            $news->title = $title;
            $news->content = $content;
            $news->newsletter_id = $newsletter->id;
            $news->save();
        }

        return $news;
    }

    public function disable_subscription($id_or_model_subscription)
    {
        $is_disabled = false;

        $subscription = $this->_instantiate_if_id($id_or_model_subscription, Subscription::class);
        if( $subscription )
        {
            $subscription->is_active = false;
            $subscription->save();
            $is_disabled = true;
        }

        return $is_disabled;
    }

    public function get_users_of_news($id_or_model_news)
    {
        $users = [];

        $news = $this->_instantiate_if_id($id_or_model_news, News::class);
        if( $news )
        {
            $newsletter = $news->newsletter;
            $subscriptions = Subscription::where('newsletter_id', $newsletter->id)
                ->where('is_active', true)
                ->get();

            foreach( $subscriptions as $subscription )
            {
                $user = $subscription->user;
                if( $user->created_at <= $news->created_at)
                    $users[] = $user;
            }
        }

        return $users;
    }
}
