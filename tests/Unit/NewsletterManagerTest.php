<?php

namespace Tests\Unit;

use App\Models\News;
use App\Models\Newsletter;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NewsletterManagerTest extends TestCase
{
    use DatabaseMigrations;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $loader = AliasLoader::getInstance();
        $loader->alias('NewsletterManager', '\App\Facades\NewsletterManagerFacade');
    }

    //
    public function testBasic()
    {
        $exist = \NewsletterManager::test_me();
        $this->assertEquals('newsletter_manager', $exist);
    }

    public function testAddUser()
    {
        $this->seed();

        $email = 'test@test.com';
        $user = \NewsletterManager::add_user($email);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($user->email, $email);

        $subscription = Subscription::where('user_id', $user->id)->first();
        $this->assertInstanceOf(Subscription::class, $subscription);
        $this->assertEquals($subscription->user_id, $user->id);
    }

    public function testCreate()
    {
        $this->seed();

        $news = \NewsletterManager::create('test T', 'testC' );

        $this->assertInstanceOf(Newsletter::class, $news);
        $this->assertEquals($news->title, 'test T');
        $this->assertEquals($news->description, 'testC');
        $this->assertEquals($news->identity, 'test-t'); // automatic title slugify
        $this->assertNotNull($news->created_at);
    }

    public function testCreateNews()
    {
        $this->seed();

        $news = \NewsletterManager::create_news('test T', 'testC', 'default');

        $this->assertInstanceOf(News::class, $news);
        $this->assertEquals($news->title, 'test T');
        $this->assertEquals($news->content, 'testC');
        $this->assertEquals($news->identity, 'test-t'); // automatic title slugify
        $this->assertNotNull($news->created_at);
    }

    public function testGetUsersOfNews()
    {
        $this->seed();

        // user
        $email = 'test@test1.com';
        $user_1 = \NewsletterManager::add_user($email);

        $email = 'test@test2.com';
        $user_2 = \NewsletterManager::add_user($email);

        // news
        $news = \NewsletterManager::create_news('test', 'test');

        // postuser
        sleep(1);
        $email = 'test@test3.com';
        $user_3 = \NewsletterManager::add_user($email);

        // get
        $users = \NewsletterManager::get_users_of_news($news);

        // test
        $this->assertNotEmpty($users);
        $this->assertCount(2, $users);
    }

    public function testAddSubscription()
    {
        $user = User::factory()->create();

        $newsletter = Newsletter::factory()->create();

        $subscription = \NewsletterManager::add_subscription($user, $newsletter);

        $this->assertInstanceOf(Subscription::class, $subscription);
        $this->assertEquals($subscription->user_id, $user->id);
        $this->assertEquals($subscription->newsletter_id, $newsletter->id);
        $this->assertTrue($subscription->is_active);
    }

    public function testDisableSubscription()
    {
        $user = User::factory()->create();

        $newsletter = Newsletter::factory()->create();

        $subscription = \NewsletterManager::add_subscription($user, $newsletter);

        // enabled
        $this->assertInstanceOf(Subscription::class, $subscription);
        $this->assertEquals($subscription->user_id, $user->id);
        $this->assertEquals($subscription->newsletter_id, $newsletter->id);
        $this->assertTrue($subscription->is_active);

        // disabeld
        $is_disabled = \NewsletterManager::disable_subscription($subscription);
        $this->assertTrue($is_disabled);

        $subscription->refresh();

        $this->assertInstanceOf(Subscription::class, $subscription);
        $this->assertEquals($subscription->user_id, $user->id);
        $this->assertEquals($subscription->newsletter_id, $newsletter->id);
        $this->assertFalse($subscription->is_active);
    }


}
