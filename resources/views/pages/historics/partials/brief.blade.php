<section id="page-breadcrumb">
    <div class="vertical-center sun">
        <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">Notre histoire</h1>
                        <p>Notre projet a commencé il y a bien longtemps…</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
