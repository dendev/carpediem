<?php
/**
 * PHP versions 7.4
 * @Copyright Formatux.be.
 * @Author: G² <info@formatux.be>
 * Date: 20/02/22
 * Description :
 *
 */
return [
    'facebook' => [
        'app_id' => env('FACEBOOK_APP_ID'),
        'app_secret' => env('FACEBOOK_APP_SECRET'),
    ],
];
