<?php

namespace App\Templates;

trait Simple
{
    private function simple()
    {
        // metas
        $this->crud->addField([   // CustomHTML
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>' . trans('backpack::pagemanager.metas') . '</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'meta_title',
            'label' => trans('backpack::pagemanager.meta_title'),
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'meta_description',
            'label' => trans('backpack::pagemanager.meta_description'),
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'meta_keywords',
            'type' => 'textarea',
            'label' => trans('backpack::pagemanager.meta_keywords'),
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>' . trans('backpack::pagemanager.content') . '</h2><hr>',
        ]);

        // page
        $this->crud->addField([
            'name' => 'title',
            'label' => "Titre",
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras'

        ]);

        $this->crud->addField([
            'name' => 'image',
            'label' => "chemin de l'image",
            'type' => 'text',
            'hint' => "Utilise les images présentes dans public/images",
            'fake' => true,
            'store_in' => 'extras'

        ]);

        $this->crud->addField([
            'name' => 'content',
            'label' => trans('backpack::pagemanager.content'),
            'type' => 'wysiwyg',
        ]);
    }
}
