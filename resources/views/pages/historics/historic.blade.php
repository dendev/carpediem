@extends('layouts.app')

@section('content')
    @include('pages.historics.partials.brief')
    @include('pages.historics.partials.content')
@endsection
