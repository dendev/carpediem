<section id="map-section">
    <div class="container">
        <p><b>Téléphone :</b> 081/31.24.34</p>
        <p><b>E-mail :</b> contact@carpediemasbl.be</p>
        <br>
        <p><b>Adresse :</b> Rue du Plateau, 11 - 5100 Jambes (Namur) - Belgique.</p>
        <br>
        <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                src="https://www.openstreetmap.org/export/embed.html?bbox=4.886662960052491%2C50.44871915957476%2C4.890654087066651%2C50.4513288297628&amp;layer=mapnik"
                style="border: 1px solid black"></iframe><br />
        <small><a href="https://www.openstreetmap.org/#map=18/50.45002/4.88866">Afficher une carte plus
                grande</a></small>
        <br><br>

        <p><b>Nos réseaux sociaux :</b></p>
        <ul class="nav nav-pills">
            <li><a href="https://www.facebook.com/Carpediemasbljambes/" target="_blank"><i class="fa fa-facebook"></i>
                    Facebook</a></li>
            <li><a href="https://www.instagram.com/carpediemasbljambes/?hl=fr" target="_blank"><i class="fa fa-instagram"></i>
                    Instagram</a></li>
            <li><a href=""><i class="fa fa-linkedin" target="_blank"></i> LinkedIn</a></li>
        </ul>

    </div>
</section>
