<?php

return [
    'permissions' => [
        'newsletter' => [
                'list' => 'newsletter_list',
                'create' => 'newsletter_create',
                'update' => 'newsletter_update',
                'show' => 'newsletter_show',
                'delete' => 'newsletter_delete',
            ],
        'news' => [
            'list' => 'news_list',
            'create' => 'news_create',
            'update' => 'news_update',
            'show' => 'news_show',
            'delete' => 'news_delete',
        ],
        'subscription' => [
            'list' => 'subscription_list',
            'create' => 'subscription_create',
            'update' => 'subscription_update',
            'show' => 'subscription_show',
            'delete' => 'subscription_delete',
        ],
    ],
    'roles' => [
        'newsletter' => [
            'newsletter_admin' => ['newsletter_list', 'newsletter_create', 'newsletter_update', 'newsletter_show', 'newsletter_delete'],
            'newsletter_viewer' => ['newsletter_list', 'newsletter_show' ],
            'newsletter_writer' => ['newsletter_list', 'newsletter_show', 'newsletter_create', 'newsletter_update' ],
        ],
        'news' => [
            'news_admin' => ['news_list', 'news_create', 'news_update', 'news_show', 'news_delete'],
            'news_viewer' => ['news_list', 'news_show' ],
            'news_writer' => ['news_list', 'news_show', 'news_create', 'news_update' ],
        ],
        'subscription' => [
            'subscription_admin' => ['subscription_list', 'subscription_create', 'subscription_update', 'subscription_show', 'subscription_delete'],
            'subscription_viewer' => ['subscription_list', 'subscription_show' ],
            'subscription_writer' => ['subscription_list', 'subscription_show', 'subscription_create', 'subscription_update' ],
        ],
    ]
];
