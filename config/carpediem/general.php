<?php

return [
    'permissions' => [
        'backpack_access',
    ],
    'roles' => [
            'admin' => [],
            'subscriber' => ['backpack_access'],
            'demo' => ['backpack_access'],
    ]
];
