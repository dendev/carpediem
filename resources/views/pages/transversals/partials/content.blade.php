<section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <!-- <h1 class="margin-bottom">Les projets transversaux</h1> -->
                <div class="text-justify">

                    <p>
                        Il nous semblait intéressant outre les projets propres à chaque service de mettre en
                        place des projets fédérateurs permettant la cohésion et le lien…</p>
                    <br>
                    <h2>Premier projet</h2>
                    <img src="images/photos/ferme.jpg" id="ferme" class="margin-bottom left" alt="ferme">
                    <p> Notre premier projet : <b>un jardin partagé et une mini-ferme</b></p>
                    <p>Carpe Diem dispose d'un espace vert que nous souhaitons mettre au service d'un projet
                        inclusif, intergénérationnel et favorisant le bien vivre ensemble. </p>
                    <p>La mise en place d'un jardin partagé et d'une mini ferme nous a donc semblé être une
                        évidence !</p>

                    <p>Un jardin partagé où nous accueillerons des personnes de tout âge, de tout horizon qui
                        souhaitent s’investir dans un potager et surtout profiter de moments de partage et
                        d’échange. Différentes parcelles seront créées afin que chacun puisse faire grandir son
                        jardin. Afin de pouvoir permettre l’accès aux personnes à mobilité réduite, des jardins
                        suspendus seront créés par les bénéficiaires. De plus, nous sommes sensibles à la
                        gestion de nos déchets et souhaitons participer à une démarche écologique, c’est
                        pourquoi un compost sera mis en place, ainsi que différentes activités autour de cette
                        thématique. </p>
                    <p>Les bienfaits de la relation avec l’animal sont nombreux et nous souhaitons pouvoir en
                        bénéficier et en faire bénéficier nos partenaires grâce à la création d’une mini-ferme.
                        Les animaux accueillis (poules, lapins, chèvres…) seront soignés par les bénéficiaires
                        avec l’aide de l’équipe et de bénévoles. Cette responsabilité tenue par les usagers leur
                        permettra de tenir un rôle valorisé et reconnu au sein de la société. </p>
                    <p>Enfin, nous rêvons de pouvoir construire un lieu de rencontre, d’accueil et d’échange
                        entre Carpe Diem, nos partenaires et toute personne qui le souhaite. Un lieu où les
                        différences seront des forces et où chacun aura sa place. Un bâtiment sera donc
                        construit pour y accueillir l’animation de différentes activités sociales, culturelles
                        et éducatives. </p>

                    <h2>

                        Nos objectifs
                    </h2>

                    <p>

                        Au cœur du quartier de Géronsart, à Jambes, ce super projet pourra bénéficier à tous nos
                        voisins, à nos partenaires ainsi qu’à tous ceux qui souhaitent se joindre à nous.
                        Ensemble, nous allons créer un projet citoyen qui se veut inclusif, intergénérationnel
                        et favorisant le bien vivre ensemble.
                    </p>

                    <h3>
                        Inclusion
                    </h3>
                    <p>

                        Nous voulons accueillir tout citoyen désireux de participer de près ou de loin à ce
                        projet. Le jardin ainsi que la mini-ferme seront accessibles à tous, sans exception,
                        selon un principe d’égalité de droit. Nous souhaitons un projet qui valorise notre droit
                        à la participation sociale. C’est pourquoi nous désirons également joindre des activités
                        de service auprès d’autres acteurs de la société. En effet, la réalisation d’une
                        activité citoyenne présente de nombreux avantages pour nous tous (inclusion-asbl.be) :
                    </p>
                    <ul>
                        <li>être valorisé, être reconnu au travers d’un statut social positif et dynamique ;
                        </li>
                        <li>maintenir et développer ses connaissances ;</li>
                        <li>développer l’autonomie par de nouveaux apprentissages / de nouvelles expériences ;
                        </li>
                        <li>valoriser ses droits et devoirs en tant que citoyen ;</li>
                        <li>développer des notions de confiance, entraide, solidarité, responsabilisation ;
                        </li>
                        <li>développer son réseau, rencontrer de nouvelles personnes ;</li>
                        <li>garder un rythme, des projets, des objectifs. </li>
                    </ul>


                    <h3>
                        Bien vivre ensemble
                    </h3>
                    <p>
                        Nous souhaitons via ce projet, favoriser la rencontre dans le quartier de Géronsart et
                        alentours. Le retour à la nature et le lien avec l’animal sont des moyens de partager
                        des
                        valeurs humaines, de développer la solidarité, la coopération, d’apprendre à se
                        connaitre,
                        avec bienveillance et respect de nos diversités sociales et culturelles.</p>
                    <h3>

                        Intergénérationnel
                    </h3>
                    <p>
                        Les projets intergénérationnels ont pour finalité de renforcer les liens entre
                        différentes
                        générations à travers des réalisations et des activités communes. Les partenariats avec
                        des
                        écoles ainsi que des maisons de repos nous paraissent essentiels pour faire vivre ce
                        projet.
                        En effet, nous souhaitons un lieu de partage et de transmission de savoir-faire qui
                        donne
                        lieu à des rencontres conviviales et qui contribue au bien vivre ensemble.</p>
                    <br>
                    <h2>Deuxième projet</h2>
                    <p>
                        Notre deuxième projet : <b>la gestion des déchets</b></p>
                    <p>Ce projet viendra en complément de notre projet jardin partagé et mini-ferme. Ce rôle de
                        citoyen responsable nous voulons l’implanter avec les usagers, les résidents, les
                        travailleurs et toute personne participant à la vie de Carpe Diem. Nous souhaitons
                        sensibiliser chacun à l’importance du tri des déchets et du respect de l’environnement
                        et du
                        lieu dans lequel nous vivons.</p>


                </div>
            </div>
        </div>
    </div>
</section>
