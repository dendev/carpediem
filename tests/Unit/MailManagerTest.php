<?php

namespace Tests\Unit;

use App\Models\News;
use App\Models\Newsletter;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MailManagerTest extends TestCase
{
    use DatabaseMigrations;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $loader = AliasLoader::getInstance();
        $loader->alias('MailManager', '\App\Facades\MailManagerFacade');
    }

    //
    public function testBasic()
    {
        $exist = \MailManager::test_me();
        $this->assertEquals('mail_manager', $exist);
    }

    public function testSend()
    {
        $newsletter = Newsletter::factory()->create();
        $news = News::factory([
            'newsletter_id' => $newsletter->id,
        ])->create();
        $user = User::factory()->create();
        $subscription = Subscription::factory([
            'user_id' => $user->id,
            'newsletter_id' => $newsletter->id,
            'is_active' => true,
        ])->create();

        $sent = \MailManager::send($news, []);

        $this->assertTrue($sent);
    }
}
