<section id="blog" class="padding-bottom">
    <div class="container">
        <div class="row">
            <div class="timeline-blog overflow padding-top">
                <div class="timeline-date text-center">
                    <a href="#" class="btn btn-common uppercase">1963</a>
                </div>
                <div class="timeline-divider overflow padding-bottom">
                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/1.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                                <span class="uppercase"><a href="#">21
                                                        <br><small>Sept</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Création de
                                        "L'atelier"</a></h2>
                                <p>le 21 septembre 1963, l’asbl « L’Atelier » a été créée et en octobre s’ouvre
                                    un atelier protégé pour jeunes hommes avec une déficience mentale.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 padding-left padding-top arrow-left wow fadeInRight"
                         data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/2.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#"> <br><small>1964</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php#">Aide</a></h2>
                                <p>Les subsides octroyés par le Fonds National de Reclassement Social des
                                    Handicapés (F.N.R.S.H.), en décembre 64, contribuent fortement au
                                    développement de l'asbl.
                                    Des locaux sont mis gratuitement à la disposition de celle-ci par la ville
                                    et la Province de Namur.</p>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="timeline-blog overflow">
                <div class="timeline-date text-center">
                    <a href="" class="btn btn-common uppercase">1965</a>
                </div>
                <div class="timeline-divider overflow padding-bottom">
                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#"><br><small>1965</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Second atelier</a></h2>
                                <p>Deux ans plus tard, un second atelier, n’accueillant que des femmes,
                                    s’implante à Bouge.</p>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
            <div class="timeline-blog overflow">
                <div class="timeline-date text-center">
                    <a href="" class="btn btn-common uppercase">1975</a>
                </div>
                <div class="timeline-divider overflow padding-bottom">
                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                                <span class="uppercase"><a href="#"><br>juin
                                                        <small>1975</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Restructuration</a></h2>
                                <p>En juin 1975, l'atelier protégé « l'Atelier » traverse une période néfaste.
                                    Pour survivre, il doit licencier 45 ouvriers et ouvrières. Ce licenciement
                                    collectif eut un effet désastreux pour les parents. Lorsqu'on leur a signalé
                                    que « L'Atelier » ne pouvait plus rémunérer leurs enfants, ils ont répondu
                                    que le principal n'était pas de les payer, mais bien de les occuper.</p>
                            </div>


                        </div>

                    </div>
                    <div class="col-sm-6 padding-left padding-top arrow-left wow fadeInRight"
                         data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/4.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                                <span class="uppercase"><a href="#">août
                                                        <br><small>1975</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">La législation bouge</a>
                                </h2>
                                <!-- <h3 class="post-author"><a href="#">Posted by micron News</a></h3> -->
                                <p>Au mois d'août, le Moniteur Belge publiait les arrêtés d'application de
                                    l'arrêté de 1973 qui créait
                                    les centres de jour. Ces arrêtés d'application se bornaient à définir les
                                    normes architecturales et de personnel.</p>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="timeline-divider overflow padding-bottom">

                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#"><br>oct <small>1975</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Centre de jour de Namur</a>
                                </h2>
                                <p>Le 1er octobre 1975, le centre de jour ouvre ses portes à Namur. Il accueille 45
                                    personnes. Ne disposant d'aucunes directives d'ordre didactique,
                                    il fallait improviser. Le centre tâtonne, s'oriente vers des activités
                                    occupationnelles : travaux artisanaux, sports, natation, balades...
                                    et lentement se structure. Il apparaît que les activités ne sont que très peu de
                                    finalités en soi, mais sont le support de valeurs plus fondamentales :
                                    entrer en relation avec autrui, développer l'autonomie, se sentir mieux dans son
                                    corps… Des erreurs furent commises : les souhaits des personnes
                                    n’étaient pas suffisamment pris en compte. L'activité était imposée plutôt que
                                    proposée.
                                    Cependant, la philosophie du centre et son approche de la personne handicapée
                                    n'ont jamais cessé d'être remises en question.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">1984</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#"><br>jan <small>1984</small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Déménagement</a></h2>
                            <p>Le 2 janvier 1984, « L'Atelier » et le centre de jour quittent l'avenue Reine
                                Astrid à Namur
                                pour s'installer dans les anciens bâtiments de Becco, toujours sur Namur. Le
                                centre de jour est agréé pour 62 personnes.</p>

                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">1987</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#"><br><small>1987</small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Nouvelle construction</a></h2>
                            <p>En 1987, Monsieur Papa, Président, entame des démarches en vue de la construction
                                d'un nouveau bâtiment.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="timeline-blog overflow">
                <div class="timeline-date text-center">
                    <a href="" class="btn btn-common uppercase">1996</a>
                </div>
                <div class="timeline-divider overflow padding-bottom">
                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                                <span class="uppercase"><a href="#">sept
                                                        <br><small>1996</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Nouveau centre de jour</a>
                                </h2>
                                <p>Le 9 septembre 1996, le centre de jour occupe ses propres locaux en bord de
                                    Meuse, derrière l'atelier protégé.<br><br>
                                    Les objectifs de l'atelier protégé (appelé actuellement ETA - Entreprise de
                                    Travail Adapté) et du centre de jour
                                    (appelé actuellement SAJA - Service d'Accueil de Jour pour handicapés
                                    Adultes) s'avèrent différents,
                                    il est nécessaire de scinder les deux établissements et de doter le centre
                                    de jour de la couverture juridique d'une asbl
                                    (Association Sans But Lucratif).
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 padding-left padding-top arrow-left wow fadeInRight"
                         data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/4.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#">dec <br><small>17</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">"Les Ateliers""</a></h2>
                                <p>
                                    Le 17 décembre 1996 le Centre de jour, devenu ainsi indépendant de son asbl
                                    mère prend le nom de Asbl Centre de jour « Les Ateliers »
                                </p>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="timeline-blog overflow">
                <div class="timeline-date text-center">
                    <a href="" class="btn btn-common uppercase">1997</a>
                </div>
                <div class="timeline-divider overflow padding-bottom">
                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                                <span class="uppercase"><a href="#">1997
<br><small>Déc</small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Achat</a></h2>
                                <p>Le 2 décembre 1997, le centre de jour achète son domaine à l'asbl « L'Atelier
                                    ».</p>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="timeline-blog overflow">
                <div class="timeline-date text-center">
                    <a href="" class="btn btn-common uppercase">2005</a>
                </div>
                <div class="timeline-divider overflow padding-bottom">
                    <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                         data-wow-delay="300ms">
                        <div class="single-blog timeline">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb">
                                    <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#">2005<br> <small></small></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-title bold"><a href="blogdetails.php">Hébergement
                                        compélémentaire au SAJA</a></h2>
                                <p>En 2005, un appel pressant des familles vieillissantes est lancé à l’asbl
                                    afin de mettre en place un hébergement complémentaire au SAJA.</p>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">2008</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#">2008<br> <small>Nov</small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Acquisition du Couvent rue du
                                    Plateau</a></h2>
                            <p>En novembre 2008, l’asbl acquiert l’ancien couvent des pères de Scheut, rue du
                                plateau 11 à Jambes afin d’y implanter les deux structures (centre de jour et
                                centre résidentiel).
                                Durant l’année 2009, les travaux sont effectués afin de rencontrer les normes
                                exigées pour l’accueil des personnes handicapées.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->

        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">2009</a>
            </div>
        </div>


        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">2009</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#">2009<br> <small>Déc</small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Transfert du centre de
                                    jour</a></h2>
                            <p>Fin décembre 2009, le centre de jour est transféré rue du plateau et y prend ses
                                activités le 1er janvier 2010.<br>
                                L’Assemblée Générale décide de changer la dénomination de l’asbl et choisit le
                                nom de Carpe Diem asbl,
                                regroupant les deux services, SAJA et SRNA et reflétant la nouvelle philosophie
                                vers laquelle nous voulons tendre.
                            </p>

                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- </div> -->

        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">2010</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#">2010<br> <small>Mars</small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Ouverture du SRNA</a></h2>
                            <p>Au 1er mars 2010, le SRNA est ouvert et accueille les premiers résidents du lundi
                                16h
                                au vendredi 8h30.
                            </p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- </div> -->

        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">2011</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#">2011<br> <small>Sept</small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Extension des activités du
                                    SRNA</a></h2>
                            <p>Au 1er septembre 2011, le SRNA étend son activité sur l’ensemble de la semaine.
                                Il
                                est donc ouvert en complément du SAJA 24h/24.
                            </p>

                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- </div> -->

        <div class="timeline-blog overflow">
            <div class="timeline-date text-center">
                <a href="" class="btn btn-common uppercase">2020</a>
            </div>
            <div class="timeline-divider overflow padding-bottom">
                <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="300ms">
                    <div class="single-blog timeline">
                        <div class="single-blog-wrapper">
                            <div class="post-thumb">
                                <!-- <img src="images/blog/timeline/3.jpg" class="img-responsive" alt=""> -->
                                <div class="post-overlay">
                                    <span class="uppercase"><a href="#">2020<br> <small></small></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="blogdetails.php">Ouverture de studios</a></h2>
                            <p>En 2020, le SRNA étend son offre de service en ouvrant 8 studios de mise en
                                autonomie.
                            </p>

                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- </div> -->
    </div>
</section>
