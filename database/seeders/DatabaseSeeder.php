<?php

namespace Database\Seeders;

use App\Models\Newsletter;
use Backpack\PageManager\app\Models\Page;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(NewsletterSeeder::class);
        $this->call(PageSeeder::class);
    }
}
