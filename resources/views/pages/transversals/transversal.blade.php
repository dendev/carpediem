
@extends('layouts.app')

@section('content')
    @include('pages.transversals.partials.title')
    @include('pages.transversals.partials.content')
@endsection
