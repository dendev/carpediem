<?php

namespace Database\Factories;

use App\Models\Newsletter;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SubscriptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $newsletter = Newsletter::factory()->create();
        $user = User::factory()->create();

        return [
            'newsletter_id' => $newsletter->id,
            'user_id' => $user->id,
            'is_active' => true,
        ];
    }
}
