
@extends('layouts.app')

@section('content')
    @include('pages.sajas.partials.title')
    @include('pages.sajas.partials.content')
    @include('pages.sajas.partials.partner')
@endsection
