<section id="home-slider">
    <div class="container">
        <div class="row">
            <div class="main-slider">
                <div class="slide-text">
                    {!! $page->content !!}

                </div>

                <img src="{{asset($page->image)}}" class="slider-house" alt="valeurs">
            </div>
        </div>
    </div>
    <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
</section>
