<?php

namespace Database\Seeders;

use Backpack\PageManager\app\Models\Page;
use Backpack\Settings\app\Models\Setting;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pages')->truncate();

        $datas = [
            [
                'template' => 'home',
                'name' => 'home',
                'title' => 'Carpe Diem asbl',
                'slug' => 'home',
                'content' => "<p>Carpe Diem asbl est un lieu d&rsquo;accueil et d&rsquo;h&eacute;bergement pour personnes adultes en situation de handicap intellectuel dans la r&eacute;gion de Namur.<br />
<br />
Un service &agrave; taille humaine, soucieux du projet de vie de chacun.</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":"images\/home\/slider\/slider.jpg"}'
            ],
            [
                'template' => 'simple',
                'name' => 'qui',
                'title' => 'Qui sommes-nous ?',
                'slug' => 'qui',
                'content' => "<p>Carpe diem asbl est un service d&#39;accueil et d&#39;h&eacute;bergement pour adultes en situation d&#39;handicap intellectuel. Le SAJA (service d&#39;accueil de jour) initialement appel&eacute; &laquo;&nbsp;Les Ateliers&nbsp;&raquo; a vu le jour en 1975.</p>

<p><br />
<img alt='groupe' src='http://127.0.0.1/carpediemOLD/images/photos/groupe.jpg' /></p>

<p>Il est agr&eacute;&eacute; pour 62 personnes et 6 personnes en convention nominative (agr&eacute;ment sp&eacute;cifique).</p>

<p>A la demande des familles vieillissantes, un SRNA (service r&eacute;sidentiel de nuit) est venu compl&eacute;ter l&#39;offre en 2010.</p>

<p>Afin de r&eacute;pondre au mieux aux besoin des b&eacute;n&eacute;ficiaires, l&#39;asbl a ouvert 8 studios de mise en autonomie en 2020 et ce pour des personnes issues de Carpe Diem ou de l&#39;ext&eacute;rieur.</p>

<p>Notre objectif est que les personnes se sentent bien, qu&#39;elles soient &eacute;panouies et qu&#39;elles puissent r&eacute;aliser leur projet de vie.</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":null}',
            ],
            [
                'template' => 'simple',
                'name' => 'mission',
                'title' => 'Notre mission ?',
                'slug' => 'mission',
                'content' => "<p><img alt='3 amis sur un banc' src='http://127.0.0.1/carpediemOLD/images/photos/about.jpg' /></p>

<p>L&rsquo;accueil et l&rsquo;h&eacute;bergement de personne en situation d&rsquo;handicap intellectuel auquel se trouvent associ&eacute;es ou non d&rsquo;autres d&eacute;ficiences.</p>

<p>Notre objectif est d&rsquo;accompagner les b&eacute;n&eacute;ficiaires dans leur projet de vie en assurant leur bien-&ecirc;tre et en valorisant leur r&ocirc;le de citoyen.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":null}',
            ],
            [
                'template' => 'simple',
                'name' => 'valeurs',
                'title' => 'Nos valeurs',
                'slug' => 'valeurs',
                'content' => null,
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":"images\/photos\/valeurs.jpg"}',
            ],
            [
                'template' => 'simple',
                'name' => 'vision',
                'title' => 'Notre vision',
                'slug' => 'vision',
                'content' => "<p><img alt='réunion de pieds' src='http://127.0.0.1/carpediemOLD/images/photos/together.jpg' /></p>

<p>D&eacute;velopper un lieu de vie &laquo; multi-services &raquo; ou l&rsquo;institution tentera d&rsquo;offrir &agrave; chaque b&eacute;n&eacute;ficiaire, gr&acirc;ce &agrave; la diversification de l&rsquo;offre, une r&eacute;ponse &agrave; ses souhaits, &agrave; ses aspirations, &agrave; ses besoins.</p>

<p>Permettre &agrave; chaque b&eacute;n&eacute;ficiaire d&rsquo;avoir la meilleure qualit&eacute; de vie possible, de participer activement &agrave; son projet de vie, d&rsquo;avoir la possibilit&eacute; d&rsquo;&ecirc;tre acteur de sa vie - y compris de sa fin de vie- et de faire des choix.</p>

<p>Cr&eacute;er un projet inclusif, interg&eacute;n&eacute;rationnel et encourager la socialisation par la rencontre d&rsquo;autres personnes dans d&rsquo;autres milieux. Utiliser les nouvelles technologies et adapter l&rsquo;environnement afin de favoriser une meilleure autonomie dans la vie quotidienne.</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":null}',
            ],
            [
                'template' => 'simple',
                'name' => 'concepts',
                'title' => 'Nos concepts',
                'slug' => 'concepts',
                'content' => "<p><strong>La Valorisation des R&ocirc;les Sociaux de Wolf WOLFENSBERGER :</strong>&nbsp;Chaque personne &agrave; un r&ocirc;le &agrave; jouer au sein de notre soci&eacute;t&eacute;. Cette valorisation passe par l&rsquo;am&eacute;lioration de l&rsquo;image de la personne et par le d&eacute;veloppement de ses comp&eacute;tences. La qualit&eacute; de vie de Robert SHALOCK la qualit&eacute; de vie d&rsquo;une personne est bonne lorsque ses besoins de base sont rencontr&eacute;s et qu&rsquo;elle a les m&ecirc;mes chances que n&rsquo;importe qui d&rsquo;autre de poursuivre et de r&eacute;aliser ses objectifs dans les diff&eacute;rents milieux de vie.</p>

<p><strong>L&rsquo;autod&eacute;termination :</strong>&nbsp;la possibilit&eacute; de pouvoir faire des choix et prendre des d&eacute;cisions en accord avec ses pr&eacute;f&eacute;rences, ses valeurs et ses objectifs. Il s&rsquo;agit, &eacute;galement, d&rsquo;un apprentissage via l&rsquo;exp&eacute;rimentation, les erreurs, la prise de risques positifs mesur&eacute;s et ce tout au long de la vie.</p>

<p><strong>L&rsquo;inclusion :</strong>&nbsp;Il s&rsquo;agit de donner sa place de citoyen &agrave; une personne en situation d&rsquo;handicap. L&rsquo;inclusion vise aussi &agrave; lever les obstacles &agrave; l&rsquo;accessibilit&eacute; pour tous aux structures ordinaires d&rsquo;enseignement, de sant&eacute;, d&rsquo;emploi, de services sociaux, de loisirs, etc.</p>

<p><strong>L&rsquo;interg&eacute;n&eacute;rationnel :</strong>&nbsp;l&rsquo;objectif est de renforcer les liens entre les g&eacute;n&eacute;rations &agrave; travers le partage d&rsquo;activit&eacute;s communes, qu&rsquo;il s&rsquo;agisse de personnes &acirc;g&eacute;s, d&rsquo;enfants, d&rsquo;adolescents, d&rsquo;adultes sans tenir compte de leurs origines ou de leurs situations.</p>

<p><strong>Le Bien vivre ensemble :</strong>&nbsp;favoriser la rencontre entre des personnes vivant dans le m&ecirc;me quartier autour d&rsquo;initiatives culturelles, festives ou de toute autre nature favorisant les &eacute;changes, la solidarit&eacute;, la coop&eacute;ration, l&rsquo;entraide dans un climat bienveillant et respectueux de nos diversit&eacute;s sociales et culturelles.</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":"images\/photos\/concepts.jpg"}',
            ],
            [
                'template' => 'simple',
                'name' => 'srna',
                'title' => 'Le SRNA',
                'slug' => 'srna',
                'content' => "<p>Le SRNA, service r&eacute;sidentiel de nuit pour adultes (service SAPS) est agr&eacute;&eacute; par l&rsquo;AViQ. Il accueille 34 personnes &agrave; partir de 18 ans. Le service se veut compl&eacute;mentaire &agrave; une activit&eacute; de jour (centre de jour, travail, b&eacute;n&eacute;volat).</p>

<p>Nous souhaitons que chaque personne puisse r&eacute;aliser son projet de vie, que ce soit dans la partie h&eacute;bergement &laquo; chambres &raquo; ou la partie &laquo; studios &raquo; qui vient d&rsquo;&ecirc;tre ouverte en 2020. Nous essayons d&rsquo;adapter au mieux notre offre de service aux souhaits et comp&eacute;tences de chacun en individualisant au maximum notre accompagnement.</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":"images\/photos\/srna.jpg"}',
            ],
            [
                'template' => 'simple',
                'name' => 'evenements',
                'title' => 'Nos événements',
                'slug' => 'evenements',
                'content' => "<p style='text-align: center'><a href='https://www.facebook.com/pg/Carpediemasbljambes/events/'>Nos &eacute;v&eacute;nements sur la page Facebook de l&#39;ASBL</a></p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":"images\/photos\/facebook.jpg"}'
            ],
            [
                'template' => 'simple',
                'name' => 'dons',
                'title' => 'Dons',
                'slug' => 'dons',
                'content' => "<p>Vous voulez soutenir et participer &agrave; un projet pla&ccedil;ant l&rsquo;Humain au centre et favorisant des projets innovants et respectueux de tous ?</p>

<p>Vous pouvez faire un don pour soutenir ces diff&eacute;rents projets via notre compte bancaire : &nbsp;&nbsp;<strong>BE18 2500 2632 3865</strong>.</p>

<p>Vous b&eacute;n&eacute;ficierez d&#39;une r&eacute;duction d&rsquo;imp&ocirc;ts pour les dons de 40&euro; minimum.</p>

<p><br />
<img alt='' src='http://127.0.0.1/carpediemOLD/images/photos/portraits-pt.png' style='width:100%' /></p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":null}'
            ],
            [
                'template' => 'simple',
                'name' => 'legs',
                'title' => 'Legs',
                'slug' => 'legs',
                'content' => "<p>Vous souhaitez l&eacute;guer vos biens ou une partie de vos biens &agrave; notre association ?</p>

<p>De votre vivant, par testament, vous pouvez soutenir nos projets...</p>

<p>Pour nous aider, prenez contact avec votre notaire qui vous informera et vous conseillera.</p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":null}'
            ],
            [
                'template' => 'simple',
                'name' => 'benevolat',
                'title' => 'Bénévolat',
                'slug' => 'benevolat',
                'content' => "<p>Vous avez du temps et des comp&eacute;tences particuli&egrave;res ?</p>

<p>Vous pouvez nous rejoindre comme b&eacute;n&eacute;vole !</p>

<p>Nous vous promettons la fatigue, la convivialit&eacute;, les moments d&rsquo;&eacute;change et du sens &agrave; nos actions.</p>

<p>Inscription via mail ou&nbsp;<a href='https://www.facebook.com/Carpediemasbljambes/'>notre page Facebook.</a></p>",
                'extras' => '{"meta_title":null,"meta_description":null,"meta_keywords":null,"image":"mages\/photos\/benevolat.jpg"}'
            ],
        ];

        foreach( $datas as $data)
        {
            $extras = json_decode($data['extras']);

            $page = new Page();
            $page->template = $data['template'];
            $page->name = $data['name'];
            $page->title = $data['title'];
            $page->slug = $data['slug'];
            $page->content = $data['content'];
            $page->extras = $extras;
            $page->save();
        }
    }
}
