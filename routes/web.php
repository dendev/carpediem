<?php

use App\Http\Controllers\Admin\FacebookCrudController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'home'])->name('pages.home');
Route::get('/histoire', [PageController::class, 'histoire'])->name('pages.historic');
Route::get('/saja', [PageController::class, 'saja'])->name('pages.saja');
Route::get('/projets-transversaux', [PageController::class, 'transversal'])->name('pages.transversal');
Route::get('/contact', [PageController::class, 'contact'])->name('pages.contact');
Route::post('/newsletter/subscribe', [PageController::class, 'newsletter_subscribe'])->name('pages.newsletter.subscribe');
Route::get('/newsletter/unsubscribe/{uid}', [PageController::class, 'newsletter_unsubscribe'])->name('pages.newsletter.unsubscribe');

Route::get('test', function (){
    \MailManager::send(1);
});

/** CATCH-ALL ROUTE for Backpack/PageManager - needs to be at the end of your routes.php file  **/
Route::get('{page}/{subs?}', ['uses' => '\App\Http\Controllers\PageController@index'])
    ->where(['page' => '^(((?=(?!admin))(?=(?!\/)).))*$', 'subs' => '.*']);
/**
 * Route Facebook
 */
Route::group(['prefix' => 'auth/facebook', 'middleware' => 'auth'], function () {
    Route::get('/', [FacebookCrudController::class, 'redirectToProvider'])->name('facebook.redirect');
    Route::get('/callback', [FacebookCrudController::class, 'handleProviderCallback'])->name('facebook.callback');
});
