<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class NewsletterManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'newsletter_manager';
    }
}
