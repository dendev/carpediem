<?php

use Doctum\Doctum;
use Symfony\Component\Finder\Finder;
use Doctum\RemoteRepository\GitHubRemoteRepository;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('resources')
    ->exclude('tests')
    ->in('./app');


return new Doctum($iterator, [
    'title'                => 'Inscription',
    'language'             => 'fr', // Could be 'fr'
    'build_dir'            => __DIR__ . '/docs/build',
    'cache_dir'            => __DIR__ . '/docs/cache',
    'remote_repository'    => new GitHubRemoteRepository('/inscription', 'https://gitlab.com/DTM-Henallux/SiegeCentral/dev/inscription.git'),
    'default_opened_level' => 2,
]);

