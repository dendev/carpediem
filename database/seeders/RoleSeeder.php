<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        if( class_exists('Spatie\Permission\Models\Role') )
        {
            // General
            $refs = config('carpediem.general.roles', []);
            foreach( $refs as $role_name => $permissions)
            {
                $role = Role::create(['name' => $role_name]);
                $role->givePermissionTo($permissions);
                $role->save();
            }

            // Users
            $refs = config('carpediem.user.roles');
            foreach( $refs as $resource => $roles )
            {
                foreach( $roles as $role_name => $permissions)
                {
                    $role = Role::create(['name' => $role_name]);
                    $role->givePermissionTo($permissions);
                    $role->save();
                }
            }
        }
        else
        {
            \Log::error("[RoleSeeder:run] GPr01 No package spatie permission found. Install it!", []);
        }
    }
}

/*
fix : Spatie\Permission\Exceptions\RoleAlreadyExists
-> php artisan cache:clear
*/
