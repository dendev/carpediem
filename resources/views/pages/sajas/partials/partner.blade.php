<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a class="jet-inline-svg jet-inline-svg--custom-width"
                           href="https://compagnonsbatisseurs.be/" target="_blank">
                            <img src="images/logos/compagnons-batisseurs.jpg" alt="Compagnons bâtisseurs"
                                 height="70">
                    </div>
                    <h2><a class="jet-inline-svg jet-inline-svg--custom-width"
                           href="https://compagnonsbatisseurs.be/" target="_blank">Les Compagnons bâtisseurs</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <a href="http://www.evasionasbl.be/" target="_blank"><img src="images/logos/none.png"
                                                                                  alt="EVASION asbl" height="70"></a>
                    </div>
                    <h2><a href="http://www.evasionasbl.be/" target="_blank">EVASION asbl</a></h2>
                    <p><a href="https://www.facebook.com/evasions.asbl/"> Evasions ASBL | Facebook</a></p>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="http://www.alteoasbl.be/" target="_blank"><img src="images/logos/alteo.jpg"
                                                                                alt="Alteo" height="70"></a>
                    </div>
                    <h2><a href="http://www.alteoasbl.be/" target="_blank">ALTEO</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="https://www.rocevasion.be/" target="_blank"><img
                                src="images/logos/rocevasion.png" alt="Roc Evasion" height="70"></a>
                    </div>
                    <h2><a href="https://www.rocevasion.be/" target="_blank">Roc Evasion</a></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="https://www.namur.be/fr/annuaire/centre-namurois-des-sports"
                           target="_blank"><img src="images/logos/none.png" alt="tabora" height="70"></a>
                    </div>
                    <h2><a href="https://www.namur.be/fr/annuaire/centre-namurois-des-sports"
                           target="_blank">Salle de sport Tabora</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <a href="https://www.bowlingnamur.be/" target="_blank"><img
                                src="images/logos/bowling.jpg" alt="Bowling de Namur" height="70"></a>
                    </div>
                    <h2><a href="https://www.bowlingnamur.be/" target="_blank">Bowling de Namur</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="https://www.ciney.be/annuaire/piscine-communale" target="_blank"><img
                                src="images/logos/none.png" alt="Piscine Ciney" height="70"></a>
                    </div>
                    <h2><a href="https://www.ciney.be/annuaire/piscine-communale" target="_blank">Piscine communale de Ciney</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="https://fr-fr.facebook.com/pages/category/Dance---Night-Club/moulindesolieres.club.50/posts/"
                           target="_blank"><img src="images/logos/moulin-solieres.jpg" alt="Moulin de Solières"
                                                height="70"></a>
                    </div>
                    <h2><a href="https://fr-fr.facebook.com/pages/category/Dance---Night-Club/moulindesolieres.club.50/posts/"
                           target="_blank">Le Moulin de Solières</a></h2>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="" target="_blank"><img src="images/logos/none.png" alt="" height="70"></a>
                    </div>
                    <h2>Musicothérapie</h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <a href="https://lafermepacomlesautres.be/" target="_blank"><img
                                src="images/logos/ferme-pacom.webp" alt="Ferme Pac-com les autres"
                                height="70"></a>
                    </div>
                    <h2><a href="https://lafermepacomlesautres.be/" target="_blank">Ferme Pac’com les autres</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="http://www.activdog.be/" target="_blank"><img src="images/logos/activdog.jpg"
                                                                               alt="ActivDog" height="70"></a>
                    </div>
                    <h2><a href="http://www.activdog.be/" target="_blank">Activ’Dog</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="https://www.aufildeleau.info/" target="_blank"><img
                                src="images/logos/aufildeleau.png" alt="Au fil de l'eau" height="70"></a>
                    </div>
                    <h2><a href="https://www.aufildeleau.info/" target="_blank">Au fil de l’eau</a></h2>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="https://www.mateteau.be/" target="_blank"><img src="images/logos/mat&eau.png"
                                                                                alt="mat'et eau" height="70"></a>
                    </div>
                    <h2> <a href="https://www.mateteau.be/" target="_blank">Mat&eau asbl</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <a href="http://www.sportadapte.be/" target="_blank"><img src="images/logos/fema.jpg"
                                                                                  alt="FéMA" height="70"></a>
                    </div>
                    <h2><a href="http://www.sportadapte.be/" target="_blank">FéMA</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <!-- <img src="images/logos/icon3.png" alt=""> -->
                        <a href="http://www.ecoledebellevue.com/" target="_blank"><img src="images/logos/none.png"
                                                                                       alt="ecole belle vue" height="70"></a>
                    </div>
                    <h2><a href="http://www.ecoledebellevue.com/" target="_blank">École Belle-Vue</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="http://www.sonefa.be/" target="_blank"><img src="images/logos/none.png"
                                                                             alt="sonefa" height="70"></a>
                    </div>
                    <h2><a href="http://www.sonefa.be/" target="_blank">Crèche Belle-Vue</a></h2>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="300ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <a href="https://www.douceur-coteaux-mosans.be/" target="_blank"><img src="images/logos/coteaux.jpg"
                                                                                              alt="La Douceur des Coteaux Mosans" height="70"></a>
                    </div>
                    <h2> <a href="https://www.douceur-coteaux-mosans.be/" target="_blank">La Douceur des Coteaux Mosans</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="600ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <a href="http://www.asblrcr.be/" target="_blank"><img src="images/logos/none.png"
                                                                              alt="RCR" height="70"></a>
                    </div>
                    <h2><a href="http://www.asblrcr.be/" target="_blank">RCR</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <!-- <img src="images/logos/icon3.png" alt=""> -->
                        <a href="http://www.asbl-sypa.be/" target="_blank"><img src="images/logos/none.png"
                                                                                alt="Les petites maisons à Courrière" height="70"></a>
                    </div>
                    <h2><a href="http://www.asbl-sypa.be/" target="_blank">Les petites maisons à Courrière</a></h2>
                </div>
            </div>
            <div class="col-sm-3 text-center padding wow fadeIn" data-wow-duration="1000ms"
                 data-wow-delay="900ms">
                <div class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <a href="" target="_blank"><img src="images/logos/none.png"
                                                        alt="" height="70"></a>
                    </div>
                    <!-- <h2><a href="http://www.sonefa.be/" target="_blank">Crèche Belle vue</a></h2> -->
                </div>
            </div>
        </div>

        <!-- </div> -->
    </div>
</section>
