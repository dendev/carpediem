@extends('layouts.mail')

@section('title')
    Newsletter
@endsection()

@section('content')
    {!! $content !!}
@endsection

@section('btn_label')
    Site Web
@endsection

<!-- same line to prevent space in url action -->
@section('btn_url'){{route('pages.home')}}@endsection

