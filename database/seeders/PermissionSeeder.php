<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        if( class_exists('Spatie\Permission\Models\Permission') )
        {
            // General
            $refs = config('carpediem.general.permissions', []);
            foreach( $refs as $access => $permission )
                \Spatie\Permission\Models\Permission::create(['name' => $permission]);

            // Users
            $refs = config('carpediem.user.permissions');
            foreach( $refs as $ref )
                foreach( $ref as $access => $permission )
                    \Spatie\Permission\Models\Permission::create(['name' => $permission]);
        }
        else
        {
            \Log::error("[PermissionSeeder:run] GPr01 No package spatie permission found. Install it!", []);
        }
    }
}

/*
fix : Spatie\Permission\Exceptions\PermissionAlreadyExists
-> php artisan cache:clear
*/
