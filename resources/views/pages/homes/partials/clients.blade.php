<section id="clients">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.aviq.be/handicap/" target="_blank"><img src="images/logos/AViQ.jpg"
                                                                                     class="img-responsive" alt="" height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.namur.be/fr" target="_blank"><img src="images/logos/Ville-de-Namur.webp"
                                                                               class="img-responsive" alt="" height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.remeso.be/" target="_blank"><img src="images/logos/remeso.svg"
                                                                              class="img-responsive" alt="" height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.cap48.be/" target="_blank"><img src="images/logos/cap48.jpg" class="img-responsive"
                                                                             alt="" height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.loterie-nationale.be/" target="_blank"><img src="images/logos/Loterie.png"
                                                                                         class="img-responsive" alt="" height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.inclusion-asbl.be/" target="_blank"><img src="images/logos/inclusion.svg"
                                                                                      class="img-responsive" alt="" height="50"></a>
                    </div>
                </div>
            </div>


            <div class="col-sm-12">

                <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">

                    <div class="col-xs-3 col-sm-2 text-center">
                        <a href="https://www.wallonie.be/fr" target="_blank"><img src="images/logos/wallonie_v.png" class="img-responsive" alt=""
                                                                                  width="100"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="https://www.leraq.be/" target="_blank"><img src="images/logos/RAQ.png" class="img-responsive"
                                                                             alt="" height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="#"><img src="images/logos/none.png" class="img-responsive" alt=""
                                         height="50"></a>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <img src="images/logos/none.png" class="img-responsive" alt="" height="50">
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <img src="images/logos/none.png" class="img-responsive" alt="" height="50">
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <img src="images/logos/none.png" class="img-responsive" alt="" height="50">
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
