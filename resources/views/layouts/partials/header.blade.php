<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 overflow">
                @include('layouts.partials.social')
            </div>
        </div>
    </div>
    <div class="navbar navbar-inverse" role="banner">
        <div class="container">
            @include('layouts.partials.nav')
        </div>
    </div>
</header>
