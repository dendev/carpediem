<?php


namespace App\Traits;


trait UtilCrud
{
    private function _set_access($config_file)
    {
        $authorized = [];

        // get refs
        $refs = config($config_file);
        if( is_null($refs) )
        {
            $refs = [];
            \Log::error("[UtilCrud::_set_access] UCsa01 config not found", [
                'config_file' => $config_file,
                'refs' => $refs,
            ]);
        }

        // set perms
        foreach( $refs as $access => $permission)
        {
            if( backpack_user()->is_admin)
            {
                $authorized[] = $access;
            }
            else if( class_exists('Spatie\Permission\Models\Permission') )
            {
                try
                {
                    if( backpack_user()->hasPermissionTo($permission) || backpack_user()->hasRole('admin') )
                        $authorized[] = $access;
                }
                catch(\Spatie\Permission\Exceptions\PermissionDoesNotExist $e)
                {
                    \Log::error("[UtilCrud:_set_access] UCsa01 Permission not found. Create it!", [
                        'permission' => $permission,
                        'error' => $e
                    ]);
                }
            }
            else
            {
                \Log::error("[UtilCrud:_set_access] UCsa02 SpatiePermission not found. Install it!", [
                    'permission' => $permission,
                    'error' => $e
                ]);
            }

        }

        // set perms to backpack operations
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'show']);
        foreach ( $authorized as $allow )
            $this->crud->allowAccess($allow);
    }
}
