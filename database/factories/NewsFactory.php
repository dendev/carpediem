<?php

namespace Database\Factories;

use App\Models\Newsletter;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $newsletter = Newsletter::factory()->create();

        return [
            'title' => $this->faker->sentence(),
            'identity' => $this->faker->slug,
            'newsletter_id' => $newsletter->id,
            'content' => $this->faker->sentence(),
        ];
    }
}
