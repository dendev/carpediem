<?php

namespace App\Providers;

use App\Services\NewsletterManagerService;
use Illuminate\Support\ServiceProvider;

class NewsletterManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'newsletter_manager', function ($app) {
            return new NewsletterManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
