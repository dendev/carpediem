<?php
namespace App\Services;

use App\Models\News as NewsModel;
use App\Mail\News as NewsMail;
use App\Traits\UtilService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class MailManagerService
{
    use UtilService;

    public function test_me()
    {
        return 'mail_manager';
    }

    public function send($id_or_model_news, $args = [])
    {
        $sent = false;

        $news = $this->_instantiate_if_id($id_or_model_news, NewsModel::class);
        if( $news  )
        {
           $users = \NewsletterManager::get_users_of_news($news);

           if( ! $news->is_sent )
           {
               foreach ($users as $user)
               {
                   $mailable = new NewsMail($news, $user);

                   Mail::to($user)
                       ->send($mailable);

                   if (Mail::failures())
                   {
                       \Log::stack(['mails', 'stack'])->error("[MailManagerService::send] MMSs01 : Error to send mail to user", [
                           'user_mail' => $user->email,
                       ]);
                   }
                   else
                   {
                       $sent = true;

                       $news->is_sent = true;
                       $news->save();

                       \Log::stack(['mails', 'stack'])->info("[MailManagerService::send] MMSs02 : sent mail ( news ) to user ( $user->email )", [
                           'user_mail' => $user->email,
                       ]);
                   }
               }
           }
           else
           {
               \Log::stack(['mails', 'stack'])->info("[MailManagerService::send] MMSs04 : news ($news->id) already sent ( $user->email )", [
                   'user_mail' => $user->email,
                   'news_id' => $news->id
               ]);
           }
        }
        else
        {
            \Log::stack(['mails', 'stack'])->info("[MailManagerService::send] MMSs03 : no news to sent", [
            ]);
        }

        return $sent;
    }
}

