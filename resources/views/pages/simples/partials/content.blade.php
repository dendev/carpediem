@if( $page->content )
<section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-justify">
                {!! $page->content !!}
            </div>
        </div>
    </div>
</section>
@endif
