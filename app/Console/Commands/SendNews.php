<?php

namespace App\Console\Commands;

use App\Models\News;
use Illuminate\Console\Command;

class SendNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:news {news_id? : id de la news}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoi la news aux abonnées';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ok = false;

        $id = $this->argument('news_id');

        if( ! is_null($id))
        {
            $ok = \MailManager::send($id, []);
        }
        else
        {
            $list_news = News::where('is_sent', false)->get();

            $total = count( $list_news );
            $nb = 0;
            foreach( $list_news as $news)
            {
                $sent = \MailManager::send($news->id, []);

                if( $sent )
                    $nb++;
            }

            $ok = ( $total === $nb );
        }

        return ($ok) ? 0: 127;
    }
}
