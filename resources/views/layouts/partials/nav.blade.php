<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <a class="navbar-brand" href="{{route('pages.home')}}">
        <h1><img src="{{asset('images/logos/carpediem.svg')}}" alt="logo" width="240"></h1>
    </a>

</div>
<nav class="collapse navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        <!-- <li class="active"><a href="index.php">Accueil</a></li> -->
        <li class="dropdown"><a href="index.php">&Agrave; propos <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">
                <li><a href="{{url("qui")}}">Qui sommes-nous ?</a></li>
                <li><a href="{{url('histoire')}}">Historique</a></li>
                <li><a href="{{url('mission')}}">Notre mission</a></li>
                <li><a href="{{url('valeurs')}}">Valeurs</a></li>
                <li><a href="{{url('vision')}}">Vision</a></li>
                <li><a href="{{url('concepts')}}">Concepts</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#">Services <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">
                <li><a href="{{url('saja')}}">Le SAJA</a></li>
                <li><a href="{{url('srna')}}">Le SRNA</a></li>
                <li><a href="{{url('projets-transversaux')}}">Projets transversaux</a></li>
                <li><a href="{{url('evenements')}}">Événements</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="dons.php">Soutien <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">
                <li><a href="{{url('dons')}}">Dons</a></li>
                <li><a href="{{url('legs')}}">Legs</a></li>
                <li><a href="{{url('benevolat')}}">Bénévolat</a></li>
            </ul>
        </li>
        <!-- <li><a href="galerie.php">Galerie</a></li> -->
        </li>
        <li><a href="{{url('contact')}}">Contact</a></li>
    </ul>

</nav>


<!-- <div class="search">
    <form role="form">
        <i class="fa fa-search"></i>
        <div class="field-toggle">
            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
        </div>
    </form>
</div> -->
