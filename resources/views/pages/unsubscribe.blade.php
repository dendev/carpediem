@extends('layouts.app')

@section('content')
    <h1 class="mt-5">Désincription de la newsletter</h1>
    @if( $ok )
        <p>Vous êtes bien désinscrit.</p>
    @else
        <p>Une erreur est survenue lors de votre inscription.</p>
    @endif
@endsection
