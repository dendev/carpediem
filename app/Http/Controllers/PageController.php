<?php

namespace App\Http\Controllers;

use App\Models\Newsletter;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\PageManager\app\Models\Page;


class PageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function home()
    {
        $page = Page::findBySlug('home');

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        $dir = $page->template . 's.'; // views.homes.home // homes general directory, home main template

        return view('pages.' .  $dir .$page->template, $this->data);
    }

    public function histoire()
    {
        return view('pages.historics.historic');
    }

    public function saja()
    {
        return view('pages.sajas.saja');
    }

    public function transversal()
    {
        return view('pages.transversals.transversal');
    }

    public function contact()
    {
        return view('pages.contacts.contact');
    }

    public function index($slug, $subs = null)
    {
        $page = Page::findBySlug($slug);

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        $dir = $page->template . 's.'; // views.homes.home // homes general directory, home main template

        return view('pages.' .  $dir .$page->template, $this->data);
    }

    public function newsletter_subscribe(Request $request)
    {
        $email = $request->input('email');
        \NewsletterManager::add_user($email);
    }

    public function newsletter_unsubscribe($uid, $newsletter_identity = 'default') // FIXME create real uid
    {
        $user = User::find($uid);
        $newsletter = Newsletter::where('identity', $newsletter_identity)->first();

        $subscription = Subscription::where('user_id', $user->id)
            ->where('newsletter_id', $newsletter->id)
            ->first();

        $ok = \NewsletterManager::disable_subscription($subscription);

        return view('pages.unsubscribe', [
            'ok' => $ok
        ]);
    }
}
