@extends('layouts.app')

@section('content')
    @include('pages.homes.partials.content')
    @include('pages.homes.partials.clients')
@endsection
