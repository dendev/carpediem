function subscribe(event)
{
    console.log( 'test');
    event.preventDefault();

    let element = document.getElementById('newsletter-input');
    let email = element.value;

    document.getElementById('newsletter-msg').textContent = '';

    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        let msg = 'Merci'
        document.getElementById('newsletter-msg').textContent = msg;
        document.getElementById('newsletter-msg').style.color = 'green';

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        let request = jQuery.ajax({
            url: "/newsletter/subscribe",
            type: "post",
            data: {email: email}
        });
    }
    else
    {
        let msg = 'Veuillez entre un email valide'
        document.getElementById('newsletter-msg').textContent = msg;
        document.getElementById('newsletter-msg').style.color = 'red';
    }
}


export {subscribe };
