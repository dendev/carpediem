# Carpediem

## Install

```bash
git clone https://gitlab.com/dendev/carpediem.git 
cd carpediem 

cp .env.example .env
vim .env

composer install
npm install
npm run dev

php artisan migrate --seed
```

## Facebook
Modification pour facebook   
dans le .env   
dans vendor/facebook/graph-sdk/src/Facebook/Http/GraphRawResponse.php   
remplacer la ligne 107 par   
preg_match('/HTTP\/\d(?:\.\d)?\s+(\d+)\s+/',$rawResponseHeader, $match);   
lien : https://github.com/facebookarchive/php-graph-sdk/issues/1246    

## Cron

```bash
crontab -e
```

h m day month week cmd
```bash
0 0 31 11 * php /var/www/html/carpediem/artisan send:news

5 4 * * * cd /var/www/html/carpediem && ./set_perms.sh
```

## Test
Executer tous les tests
```bash
phpunit 
```

Executer un jeu de test spécifique
```bash
phpunit tests/Unit/MailManagerTest.php
```

Executer un test spécifique
```bash
phpunit --filter testSend
``
