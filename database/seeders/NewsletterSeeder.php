<?php

namespace Database\Seeders;

use App\Models\Newsletter;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class NewsletterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('newsletters')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $datas = [
            [
                'title' => 'Générale',
                'identity' => 'default',
                'description' => 'Infos générale'
            ]
        ];

        foreach( $datas as $data )
        {
            $newsletter = new Newsletter();
            $newsletter->title = $data['title'];
            $newsletter->identity = $data['identity'];
            $newsletter->description = $data['description'];
            $newsletter->save();
        }
    }
}
