require('./bootstrap');

let jQuery = require('jquery')
window.jQuery = jQuery

require('@popperjs/core')
require('bootstrap')
require('./lightbox.min')
require('./main')

// utils
import {subscribe} from "./utils";
window.subscribe = subscribe;
