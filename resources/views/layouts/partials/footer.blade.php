<footer id="footer" class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">
                <!-- <img src="#" class="img-responsive inline" alt=""> -->
            </div>
            <div class="col-md-3 col-sm-6">
                <img src="{{asset('images/QR.jpg')}}" alt="QR code" title="Scannez ce code avec l'appareil photo de votre gsm">
                <br>
                <a href="{{asset('images/logos/carpediem.jpg')}}" target="_blank">Télécharger le logo de Carpe Diem</a>
            </div>
            <div class="col-md-5 col-sm-7">
                <div class="contact-info bottom">
                    <h2>Contacts</h2>
                    <address>
                        <p>E-mail: <a href="mailto:contact@carpediemasbl.be">contact@carpediemasbl.be</a> </p>
                        <p>Téléphone: 081/31.24.34</p>
                    </address>

                    <h2>Adresse</h2>
                    <address>
                        Rue du Plateau, 11<br>
                        5100 Jambes <br>
                        Belgique<br>
                    </address>
                </div>
            </div>
            <img src="{{asset('images/photos/dots.png')}}" id="dots" alt="" style="margin-left:60px;">

            <!-- newsletter -->
        <div class="col-md-4 col-sm-12">
            <form id="newsletter">
                @csrf
                <div class="form-group">
                    <label for="newsletter-input" id="newsletter-label">Recevez notre newsletter</label>
                    <input type="email" class="form-control" id="newsletter-input" name="email" placeholder="votre email" required>
                    <button class="btn btn-outline-secondary" type="submit" onclick="subscribe(event)">Valider</button>
                    <p id="newsletter-msg"></p>

                </div>
            </form>
        </div>

            <!-- end -->
            <div class="col-sm-12">
                <div class="copyright-text text-center" style="padding-top:90px">
                    <p>&copy; Carpe Diem asbl - &copy; 2021 - <?php echo date('Y') ?></p>
                    <!-- <p>Designed by <a target="_blank" href="http://www.themeum.com">Themeum</a></p> -->
                    <br>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->
