<?php

return [
    'permissions' => [
        'user' => [
                'list' => 'user_list',
                'create' => 'user_create',
                'update' => 'user_update',
                'show' => 'user_show',
                'delete' => 'user_delete',
            ],
    ],
    'roles' => [
        'user' => [
            'user_admin' => ['user_list', 'user_create', 'user_update', 'user_show', 'user_delete'],
            'user_viewer' => ['user_list', 'user_show' ],
            'user_writer' => ['user_list', 'user_show', 'user_create', 'user_update' ],
        ],
    ]
];
